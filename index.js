let playButton = document.querySelector(".play");
let lapButton = document.querySelector(".lap");
let resetButton = document.querySelector(".reset");
let clearButton = document.querySelector(".lap-clear-button");

let minute = document.querySelector(".minute");
let second = document.querySelector(".sec");
let centiSecond = document.querySelector(".msec");
let laps = document.querySelector(".laps");


let isPlay = false;
let isReset = false;

let min;
let minCounter = 0;
let secCounter = 0;
let sec;
let centiSec;
let centiCounter = 0;

let toggleButton = function () {
    lapButton.classList.remove("hidden");
    resetButton.classList.remove("hidden");
}
 
let play = function () {

    if (!isPlay && !isReset) {
        playButton.innerHTML = "Pause";
        min = setInterval(function () {
            minute.innerHTML = `${++minCounter} : `;
        }, 60 * 1000);
        sec = setInterval(function () {
            if (second === 60) {
                secCounter = 0;
            }
            second.innerHTML = `${++secCounter} : `;
        }, 1000);
        centiSec = setInterval(function () {
            if (centiCounter === 100) {
                centiCounter = 0;
            }
            centiSecond.innerHTML = `${++centiCounter}`;
        }, 10);
        isPlay = true;
        isReset = true;
    } else {
        playButton.innerHTML = "Play";
        clearInterval(min);
        clearInterval(sec);
        clearInterval(centiSec);
        isPlay = false;
        isReset = false;
    }
    toggleButton();
}

let reset = function () {
    isReset = true;
    play();
    lapButton.classList.add("hidden");
    resetButton.classList.add("hidden");
    minute.innerHTML = "0 :";
    second.innerHTML = "0 :";
    centiSecond.innerHTML = "0";
}

let lap = function () {
    let li = document.createElement("li");
    let number = document.createElement("span");
    let timeStamp = document.createElement("span");

    li.setAttribute("class", "lap-item");
    number.setAttribute("class", "number");
    timeStamp.setAttribute("class", "time-stamp");

    timeStamp.innerHTML = `${minCounter} : ${secCounter} : ${centiCounter}`;

    li.append(number, timeStamp);
    laps.append(li);
}

let clearAll = function () {
    laps.innerHTML = "";
    laps.append(clearButton);
}
playButton.addEventListener("click", play);
resetButton.addEventListener("click", reset);
lapButton.addEventListener("click", lap);
clearButton.addEventListener("click", clearAll);